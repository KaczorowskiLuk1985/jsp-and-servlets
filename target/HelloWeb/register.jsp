<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8" %>

<%@ taglib prefix="c" uri= "http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored ="false" %>

<html>
<head>
    <title>Register</title>
</head>
<body>
<jsp:include page="menu.jsp"/>

<c:if test="${requestScope.registerMessage !=null}">
${requestScope.registerMessage}
</c:if>


<form method="POST" action = "register">
    <table>
        <tr>
            <td>Login</td>
            <td><input type= "text" name="login"/></td>
        </tr>

         <tr>
             <td>Password</td>
             <td><input type= "password" name="password"/></td>
         </tr>

         <tr>
             <td>Retype Password</td>
             <td><input type= "password" name="retypePassword"/></td>
         </tr>

         <tr>
              <td>Register</td>
              <td><input type="submit" value="SEND!"/></td>
         </tr>
    </table>
</form>
</body>
</html>