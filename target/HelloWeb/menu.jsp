<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>

<c:if test="${sessionScope.userLogin !=null}">
    <p style="color: green"> Hello, <c:out value="${sessionScope.userLogin}"/></p>
</c:if>

<head>
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #333;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover {
            background-color: #111;
        }
    </style>
</head>
<ul>
    <li><h4><a href="index">Home</a></h4></li>
    <li><h4><a href="gallery">Gallery</a></h4></li>
    <li><h4><a href="about">About</a></h4></li>
    <li><h4><a href="contact">Contact</a></h4></li>
    <li><h4><a href="instagram">Instagram</a></h4></li>
    <li><h4><a href="showPersons">Show persons</a></h4></li>
    <li><h4><a href="museums">Museums</a></h4></li>
    <li><h4><a href="addPerson">Add Person</a></h4></li>
    <c:if test="${sessionScope.userLogin ==null}">
        <li style="float:right"><h4><a href="login">Login</a></h4></li>
        <li style="float:right"><h4><a href="register">Register</a></h4></li>
    </c:if>
    <c:if test="${sessionScope.userLogin !=null}">
        <li style="float:right"><h4><a href="logout">Logout</a></h4></li>
    </c:if>

</ul>


<%-- sadasd --%>