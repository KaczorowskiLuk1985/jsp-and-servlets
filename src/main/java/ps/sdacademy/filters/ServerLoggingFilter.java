package ps.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;

@WebFilter(filterName = "ServerLoggingFilter", urlPatterns = {"/*"})
public class ServerLoggingFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        HttpServletRequest request1 = (HttpServletRequest) request;
        System.out.println("*****************************************************************************************");
        log(request1.getRequestURI());
        log(request1.getRequestURL().toString());
        log(request1.getMethod());
        request1.getParameterMap().entrySet().stream().forEach(parameter -> {
            log("Name: " + parameter.getKey() + ", Value: " + Arrays.toString(parameter.getValue()));
        });

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

    private void log(String info) {
        System.out.println("{ServerLoggingFilter}" + info);
    }
}
