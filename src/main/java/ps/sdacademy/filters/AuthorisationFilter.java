package ps.sdacademy.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebFilter(filterName = "AuthorisationFilter", urlPatterns = {"/*"})
public class AuthorisationFilter implements Filter {

    private List<String> allowedUrl = new ArrayList<>(
            Arrays.asList(
                    "/index",
                    "/index.jsp",
                    "/home",
                    "/login",
                    "/login.jsp",
                    "/register",
                    "/register.jsp"
            )
    );


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        HttpSession session = ((HttpServletRequest) request).getSession(false);
        String uri = req.getRequestURI();

        if (session != null && session.getAttribute("userLogin") != null) {
            chain.doFilter(request, response);
        } else {
            if (allowedUrl.contains(uri)) {
                chain.doFilter(request, response);
            } else {
                res.sendRedirect("index.jsp");
            }
        }
    }

    @Override
    public void destroy() {

    }

    private void log(String info) {
        System.out.println("{AuthorisationFilter}" + info);
    }
}
