package ps.sdacademy.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Gallery {
    private String name;
    private List<Image> images;
    private String date;

    public Gallery(String name) {
        this.name = name;
        images = new ArrayList<>();
        date = LocalDate.now().toString();
    }
    public void addImageToList (Image img){
        images.add(img);

    }

    public String getName() {
        return name;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getDate() {
        return date;
    }
}
