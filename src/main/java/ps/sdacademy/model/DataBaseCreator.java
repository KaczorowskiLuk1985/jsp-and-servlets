package ps.sdacademy.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;

public class DataBaseCreator {
    public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {

        Connection connection = null;
        PreparedStatement preparedStatement = null;


        Class.forName("org.sqlite.JDBC");
        connection = DriverManager.getConnection("jdbc:sqlite:D:\\JAV\\sqlite\\MuseumsPL.db");
        preparedStatement = connection.prepareStatement("INSERT INTO museum VALUES (?,?,?,?,?,?,?,?,?,?,?);");


        BufferedReader bufferedReader = new BufferedReader(new FileReader("museums.csv"));
        String read = "";

        while ((read = bufferedReader.readLine()) != null) {
            if (read != null) {
                String[] array = read.split(";");
                preparedStatement.setString(1, array[0]);
                preparedStatement.setString(2, array[1]);
                preparedStatement.setString(3, array[2]);
                preparedStatement.setString(4, array[3]);
                preparedStatement.setString(5, array[4]);
                preparedStatement.setString(6, array[5]);
                preparedStatement.setString(7, array[6]);
                preparedStatement.setString(8, array[7]);
                preparedStatement.setString(9, array[8]);
                preparedStatement.setString(10, array[9]);
                preparedStatement.setString(11, array[10]);
                preparedStatement.executeUpdate();
            }
        }
    }
}



