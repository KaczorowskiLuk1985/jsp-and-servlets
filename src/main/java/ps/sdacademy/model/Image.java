package ps.sdacademy.model;

public class Image {
    private String url;
    private String description;

    public Image(String url, String note) {
        this.url = url;
        this.description = note;
    }


    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "[" +
                " description='" + description + '\'' +
                " url= '" + url + '\'' +
                ']';
    }
}
