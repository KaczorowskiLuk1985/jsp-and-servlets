package ps.sdacademy.model;

public enum Sex {
    MALE, FEMALE, UNKNOWN
}
