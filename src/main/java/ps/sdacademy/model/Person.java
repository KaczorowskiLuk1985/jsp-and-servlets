package ps.sdacademy.model;

public class Person {
    String name;
    String lastName;
    int bornYear;
    int phoneNumber;
    Sex sex;

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getBornYear() {
        return bornYear;
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public Sex getSex() {
        return sex;
    }

    public Person(String name, String lastName, int bornYear, int phoneNumber, Sex sex) {
        this.name = name;
        this.lastName = lastName;
        this.bornYear = bornYear;
        this.phoneNumber = phoneNumber;
        this.sex = sex;


    }
}

