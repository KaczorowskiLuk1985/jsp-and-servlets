package ps.sdacademy.model;

public class Museum {
    public static void main(String[] args) {

    }

    String id;
    String name;
    String postalCode;
    String city;
    String streetPrefix;
    String street;
    String houseNumber;
    String flatNumber;
    String organizer;
    String entryDate;
    String status;

    public Museum(String  id,
                  String name,
                  String postalCode,
                  String city,
                  String streetPrefix,
                  String street,
                  String houseNumber,
                  String flatNumber,
                  String organizer,
                  String entryDate,
                  String status) {
        this.id = id;
        this.name = name;
        this.postalCode = postalCode;
        this.city = city;
        this.streetPrefix = streetPrefix;
        this.street = street;
        this.houseNumber = houseNumber;
        this.flatNumber = flatNumber;
        this.organizer = organizer;
        this.entryDate = entryDate;
        this.status = status;
    }

    public String  getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getStreetPrefix() {
        return streetPrefix;
    }

    public String getStreet() {
        return street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getFlatNumber() {
        return flatNumber;
    }

    public String getOrganizer() {
        return organizer;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public String getStatus() {
        return status;
    }
}
