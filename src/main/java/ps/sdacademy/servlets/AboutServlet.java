package ps.sdacademy.servlets;

import ps.sdacademy.model.Person;
import ps.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "AboutServlet", value = "/about")

public class AboutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Person> personList = new ArrayList<>();

        Person p1 = new Person("Pior", "Ziętek", 1995, 555666555, Sex.MALE);
        Person p2 = new Person("Anna", "Kowalska", 1985, 555645555, Sex.FEMALE);
        Person p3 = new Person("Beata", "Wysznacka", 1978, 555357557, Sex.FEMALE);
        Person p4 = new Person("Jan", "Nowak", 1968, 553557557, Sex.MALE);

        personList.add(p1);
        personList.add(p2);
        personList.add(p3);
        personList.add(p4);

        request.setAttribute("person_list", personList);
        request.getRequestDispatcher("about.jsp").forward(request, response);


//        String actionValue = request.getParameter("action");
//        System.out.println(actionValue);
//        response.sendRedirect("about.jsp");
    }
}
