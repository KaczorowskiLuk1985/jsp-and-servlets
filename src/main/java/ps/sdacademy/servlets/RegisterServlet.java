package ps.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(name = "RegisterServlet", value = "/register")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String loginFromForm = request.getParameter("login");
        String passwordFromForm = request.getParameter("password");
        String retypePasswordFromForm = request.getParameter("retypePassword");

        if (passwordFromForm.equals(retypePasswordFromForm)) {
            if (register(loginFromForm, passwordFromForm) == 1) {
                String loginMessage = "<p style =\"color: green\">Successfully registered user: " + loginFromForm + "!</p>";
                request.setAttribute("loginMessage", loginMessage);
                request.getRequestDispatcher("login.jsp").forward(request, response);

            } else {
                String registerMessage = "<p style =\"color: red\">User already exist! " + loginFromForm + "</p>";
                request.setAttribute("registerMessage", registerMessage);
                request.getRequestDispatcher("register.jsp").forward(request, response);

            }

        } else {
            String registerMessage = "<p style =\"color: red\">Passwords do not match!</p>";
            request.setAttribute("registerMessage", registerMessage);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("register.jsp");
    }

    private int register(String loginFromForm, String passwordFromForm) {
        int insertRows = 0;

        try {
            Class.forName("org.sqlite.JDBC");
            Connection connection = DriverManager.getConnection("jdbc:sqlite:D:\\JAV\\sqlite\\users.db");
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO usersArr VALUES (?,?);");

            preparedStatement.setString(1, loginFromForm);
            preparedStatement.setString(2, passwordFromForm);

            insertRows = preparedStatement.executeUpdate();


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("User already exist: " + loginFromForm + ", " + e.getMessage());
        }

        return insertRows;

    }
}