package ps.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "HomeServlet", value = "/index")
public class HomeServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.sendRedirect("index.jsp"); // zapamiętać to polecenie, powoduje przekierowanie uzytkownika do konkretnej strony
//        String actionValue = request.getParameter("action");
//        System.out.println("Redirecting to " + actionValue);
        response.sendRedirect("index.jsp");
//        if (actionValue != null) {
//            switch (actionValue) {
//                case "gallery":
//                    response.sendRedirect("gallery.jsp");
//                    break;
//                case "about":
//                    response.sendRedirect("about.jsp");
//                    break;
//                case "contact":
//                    response.sendRedirect("contact.jsp");
//                    break;
//                case "index":
//                    response.sendRedirect("index.jsp");
//                    break;
//                default:
//                    System.out.println("nie poszło, porwot do home");
//                    response.sendRedirect("index.jsp");
//                    break;
//            }
//        } else {
//            System.out.println(" wpisałeś zły link");
//            response.sendRedirect("index.jsp");
//        }
    }
}