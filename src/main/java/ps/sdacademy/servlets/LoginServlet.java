package ps.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;


@WebServlet(name = "LoginServlet", value = "/login")

public class LoginServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String loginFromForm = request.getParameter("login");
        String passwordFromForm = request.getParameter("password");
        Connection connection = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:D:\\JAV\\sqlite\\users.db");
            String sql
                    = "SELECT * FROM usersArr WHERE login='" + loginFromForm + "' AND password='" + passwordFromForm + "';";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);

            ResultSet resultSet = preparedStatement.executeQuery();

            if (!resultSet.next()) {
                String loginMessage = "<p style =\"color: red\">Login or Passwords error!</p>";
                request.setAttribute("loginMessage", loginMessage);
                request.getRequestDispatcher("login.jsp").forward(request, response);

            } else {

                HttpSession session = request.getSession();
                session.setMaxInactiveInterval(2000);
                session.setAttribute("userLogin", loginFromForm);
                response.sendRedirect("index.jsp");
            }

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //wyswietlamy formularz logowania
        response.sendRedirect("login.jsp");

    }
}