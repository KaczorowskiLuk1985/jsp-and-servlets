package ps.sdacademy.servlets;

import ps.sdacademy.model.Person;
import ps.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "showPersonsServlet", value = "/showPersons")
public class ShowPersonsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<Person> testList = new ArrayList<>();

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:D:\\JAV\\sqlite\\people.db");
            preparedStatement = connection.prepareStatement("SELECT * FROM people;");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                String column1Value = resultSet.getString(1);
                String column2Value = resultSet.getString(2);
                int column3Value = resultSet.getInt(3);
                int column4Value = resultSet.getInt(4);
                String column5Value = resultSet.getString(5);

                Person person = new Person(column1Value, column2Value, column3Value, column4Value, Sex.valueOf(column5Value));
                testList.add(person);

            }
        } catch (SQLException | ClassNotFoundException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                resultSet.close();
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        request.setAttribute("person_list", testList);
        request.getRequestDispatcher("showPersons.jsp").forward(request, response);


    }
}
