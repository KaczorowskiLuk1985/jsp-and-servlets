package ps.sdacademy.servlets;

import ps.sdacademy.model.Gallery;
import ps.sdacademy.model.Image;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "GalleryServlet", value = "/gallery")

public class GalleryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Gallery gallery = new Gallery("nowaGaleria");
        Image img1 = new Image("https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/FullMoon.jpg/800px-FullMoon.jpg"
                , "ksieżyc w nowiu");
        Image img2 = new Image("https://upload.wikimedia.org/wikipedia/commons/0/09/POL_Brzez%C3%B3wka_Drzewo.JPG",
                "drzewo");
        Image img3 = new Image("https://image.shutterstock.com/image-vector/icon-structure-nucleus-atom-around-450w-785587942.jpg"
                , "model atomu");
        gallery.addImageToList(img1);

        gallery.addImageToList(img2);
        gallery.addImageToList(img3);

        request.setAttribute("nowaGaleria", gallery); //do obiektu żadania dodajemy dowolny obiekt java - nazwe wyymyslamy
        request.getRequestDispatcher("gallery.jsp").forward(request, response); // zawsze na końcu
    }
}