package ps.sdacademy.servlets;


import ps.sdacademy.model.Sex;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;

@WebServlet(name = "AddPersonsServlet", value = "/addPerson")
public class AddPersonsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String name = request.getParameter("name");
        String lastName = request.getParameter("last_name");
        String bornYear = request.getParameter("born_year");
        String phone = request.getParameter("phone");
        String sex = request.getParameter("sex");

        switch (sex.toLowerCase()) {
            case "male":
                sex = Sex.MALE.toString();
                break;
            case "female":
                sex = Sex.FEMALE.toString();
                break;
            default:
                sex = Sex.UNKNOWN.toString();
                break;
        }


        Connection connection = null;
        PreparedStatement preparedStatement = null;

        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:D:\\JAV\\sqlite\\people.db");
            preparedStatement = connection.prepareStatement("INSERT INTO people VALUES (?,?,?,?,?);");

            preparedStatement.setString(1, name);
            preparedStatement.setString(2, lastName);
            preparedStatement.setString(3, String.valueOf(Integer.parseInt(bornYear)));
            preparedStatement.setString(4, String.valueOf(Integer.parseInt(phone)));
            preparedStatement.setString(5, sex);

            preparedStatement.executeUpdate();

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        response.sendRedirect("showPersons");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("addPerson.jsp");
    }
}