package ps.sdacademy.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ContactServlet", value = "/contact")

public class ContactServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       request.setCharacterEncoding("UTF-8");

        String userName = request.getParameter("user_name");
        String userSurname = request.getParameter("user_surname");
        String userEmail = request.getParameter("user_email");
        String userMessage = request.getParameter("message");

        userMessage = userMessage.replace("\n", "<br>");

        request.setAttribute("name",userName);
        request.setAttribute("surname",userSurname);
        request.setAttribute("email",userEmail);
        request.setAttribute("message",userMessage);

        request.getRequestDispatcher("sendMessage.jsp").forward(request,response);

    }
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("contact.jsp");
    }
}