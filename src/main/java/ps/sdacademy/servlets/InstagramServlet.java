package ps.sdacademy.servlets;

import ps.sdacademy.model.Gallery;
import ps.sdacademy.utils.InstagramDownloader;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "InstagramServlet", value = "/instagram")

public class InstagramServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        String instagramName = request.getParameter("user_name");
        String numberOfPhotos = request.getParameter("number");
        int numberAsInt = Integer.valueOf(numberOfPhotos);

        InstagramDownloader instagramDownloader = new InstagramDownloader(instagramName, numberAsInt);
        Gallery gallery = instagramDownloader.downloadPhotos();

        request.setAttribute("nowaGaleria", gallery); //do obiektu żadania dodajemy dowolny obiekt java - nazwe wyymyslamy
        request.getRequestDispatcher("gallery.jsp").forward(request, response); // zawsze na końcu


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("instagram.jsp");
    }


}
