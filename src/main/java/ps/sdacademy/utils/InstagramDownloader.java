package ps.sdacademy.utils;

import org.json.JSONArray;
import org.json.JSONObject;
import ps.sdacademy.model.Gallery;
import ps.sdacademy.model.Image;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class InstagramDownloader {
    private String instagramUserName;
    private int numberOfPhotos;
    // gdy deklarujemy stałe używamy wielkich liter z podłogą
    private final String INSTAGRAM_FINAL = "https://instagram.com/";


    //kolejnosc metod - publiczne -> przywatne

    public InstagramDownloader(String instagramUserName, int numberOfPhotos) {
        this.instagramUserName = instagramUserName;
        this.numberOfPhotos = numberOfPhotos;
    }

    public Gallery downloadPhotos() {
        String pageSource = downloadPageSource();
        String jsonAsString = extractJsonAsString(pageSource);
        List<String> photoLinks = extractFotoLinksFromJson(jsonAsString);
        Gallery gallery = createGallery(photoLinks);

        return gallery;

    }

    private String downloadPageSource() {
        String source = "";

        try {
            URL url = new URL(INSTAGRAM_FINAL + instagramUserName);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            source = bufferedReader.lines().collect(Collectors.joining());


        } catch (IOException e) {
            e.printStackTrace();
        }
        return source;
    }

    private String extractJsonAsString(String pageSource) {
        String[] splitPageSource = pageSource.split("window._sharedData = ");
        String extractedJsonAsString = splitPageSource[1].split(";</script>")[0];
        return extractedJsonAsString;
    }

    private List<String> extractFotoLinksFromJson(String jsonAsString) {
        JSONObject jsonObject = new JSONObject(jsonAsString);
        JSONArray jsonArray = jsonObject.getJSONObject("entry_data")
                .getJSONArray("ProfilePage")
                .getJSONObject(0)
                .getJSONObject("graphql")
                .getJSONObject("user")
                .getJSONObject("edge_owner_to_timeline_media")
                .getJSONArray("edges");

        List<String> listOfimage = new LinkedList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            if (i < numberOfPhotos) {
                String fotoURL = jsonArray.getJSONObject(i)
                        .getJSONObject("node")
                        .getString("display_url");
                listOfimage.add(fotoURL);
            }

        }
        return listOfimage;

    }

    private Gallery createGallery(List<String> Links) {
        Gallery instagramGallery = new Gallery("Instagram photos");
        for (String link : Links) {
            Image image = new Image(link, "instaram photo");
            instagramGallery.addImageToList(image);
        }

        return instagramGallery;

    }
}
