<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri= "http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored ="false" %>

<html>
<body>
<h4> hello ${requestScope.name} ${requestScope.name}</h4>
<h4> your email:  ${requestScope.email}</h4>
<h4> your message:  ${requestScope.message}</h4>
<h4> thanks!</h4>

<h4> back to <a href= "index">Home</a></h4>

</body>
</html>