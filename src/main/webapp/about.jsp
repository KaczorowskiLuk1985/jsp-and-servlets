<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri= "http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix ="fn" uri = "http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored ="false" %>
<html>
<style>
table, th, td {
  border: 1px solid black;
}
</style>
<body>
<jsp:include page="menu.jsp"/>
<h3>ABOUT</h3>
<h2><% out.print("Hello World!?"); %></h2>

ilosc osob <c:out value = "${fn:length(requestScope.person_list)}"/>
<table style="width:50%">
  <tr>
    <th>Name</th>
    <th>Last name</th>
    <th>Born Year</th>
    <th>Phone</th>
    <th>Sex</th>
  </tr>

    <c:forEach items="${requestScope.person_list}" var="person">
    <tr>
        <td>${person.getName()}</td>
        <td>${person.getLastName()}</td>
        <td>${person.getBornYear()}</td>
        <td>${person.getPhoneNumber()}</td>
        <td>${person.getSex()}</td>
    </tr>
    </c:forEach>
<p></p>
</table>

<h2>Zad 2</h2>
<table style="width:50%">
  <tr>
    <th>Name</th>
    <th>Last name</th>
    <th>Born Year</th>
    <th>Phone</th>
    <th>Sex</th>
  </tr>

    <c:forEach items="${requestScope.person_list}" var="person">
    <c:if test="${person.getSex()=='MALE'}">
    <tr>
        <td>${person.getName()}</td>
        <td>${person.getLastName()}</td>
        <td>${person.getBornYear()}</td>
        <td>${person.getPhoneNumber()}</td>
        <td>${person.getSex()}</td>
    </tr>
    </c:if>
    </c:forEach>
        <c:forEach items="${requestScope.person_list}" var="person">
        <c:if test="${person.getSex()=='FEMALE'}">
        <tr>
            <td>${person.getName()}</td>
            <td>${person.getLastName()}</td>
            <td>${person.getBornYear()}</td>
            <td>${person.getPhoneNumber()}</td>
            <td>${person.getSex()}</td>
        </tr>
        </c:if>
        </c:forEach>
<p></p>
</table>

<h2>Zad 3 starsze niż30 lat</h2>
<table style="width:50%">
  <tr>
    <th>Name</th>
    <th>Last name</th>
    <th>Born Year</th>
    <th>Phone</th>
    <th>Sex</th>
  </tr>

    <c:forEach items="${requestScope.person_list}" var="person">
        <c:if test="${person.getBornYear() < 1989}">
            <c:choose>
                <c:when test="${fn:startsWith(person.getLastName(),'K')}">
                    <tr style="background-color: green">
                </c:when>
                <c:otherwise>
                   <tr>
                </c:otherwise>
            </c:choose>
                <td>${person.getName()}</td>
                <td>${person.getLastName()}</td>
                <td>${person.getBornYear()}</td>
                <td>${person.getPhoneNumber()}</td>
                <td>${person.getSex()}</td>
            </tr>
        </c:if>
    </c:forEach>
<p></p>
</table>





</body>
</html>