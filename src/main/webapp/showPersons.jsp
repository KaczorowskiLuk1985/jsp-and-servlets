<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>
<html>
<style>
    table, th, td {
        border: 1px solid black;
    }
</style>
<body>
<jsp:include page="menu.jsp"/>
<h3>SHOW PERSON</h3>
<h2><% out.print("Hello World!?"); %></h2>


<table style="width:50%">
    <tr>
        <th>Name</th>
        <th>Last name</th>
        <th>Born Year</th>
        <th>Phone</th>
        <th>Sex</th>
    </tr>

    <c:forEach items="${requestScope.person_list}" var="person">
        <tr>
            <td>${person.getName()}</td>
            <td>${person.getLastName()}</td>
            <td>${person.getBornYear()}</td>
            <td>${person.getPhoneNumber()}</td>
            <td>${person.getSex()}</td>
        </tr>
    </c:forEach>
    <p></p>
</table>