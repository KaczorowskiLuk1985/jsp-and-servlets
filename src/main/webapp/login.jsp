<%@ page language="java" contentType="text/html; charset=UTF-8"
        pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<html>
<head>

    <title>Login</title>
</head>
<body>
<jsp:include page="menu.jsp"/>
<c:if test="${requestScope.loginMessage !=null}">
${requestScope.loginMessage}
</c:if>

<form method="POST" action = "login">
    <table>
        <tr>
            <td>Login</td>
            <td><input type= "text" name="login"/></td>
        </tr>

         <tr>
             <td>Password</td>
             <td><input type= "password" name="password"/></td>
         </tr>

         <tr>
              <td>Submit</td>
              <td><input type="submit" value="LOG!"/></td>
         </tr>
    </table>
</form>
</body>
</html>