<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ page import="ps.sdacademy.model.Image" %>
<%@ page import="ps.sdacademy.model.Gallery" %>
<html>
<body>
<jsp:include page="menu.jsp"/>
<h3>GALLERY</h3>
<% Gallery gallery = (Gallery) request.getAttribute("nowaGaleria"); %>
<h4>Name: <%= gallery.getName() %>
</h4>
<h4>Created: <%= gallery.getDate() %>
</h4>
<br>

<table border=2 style="width:30%">
    <% for (Image img : gallery.getImages()) { %>
    <tr>
        <td>
            <img src="<%=img.getUrl()%>" tilte="<%= img.getDescription()%>" style="width:100%"/>
        </td>
    </tr>
    <% } %>
</table>
</body>
</html>